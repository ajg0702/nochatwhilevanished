package us.ajg0702.novanishchat;

import de.myzelyam.api.vanish.BungeeVanishAPI;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

public class Main extends Plugin implements Listener {

	
	@Override
	public void onEnable() {
		getProxy().getPluginManager().registerListener(this, this);
	}
	
	@EventHandler
	public void onChat(ChatEvent e) {
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		if(!BungeeVanishAPI.isInvisible(p))  return;
		
		
		String message = e.getMessage();
		if(message.indexOf('/') == 0) {
			return;
		}
		if(message.indexOf('\\') == 0) {
			e.setMessage(message.replaceFirst("\\\\", ""));
			return;
		}
		
		
		e.setCancelled(true);
		p.sendMessage(formatMessage("&cYou cannot talk while vanished! &7To bypass, start your message with '\\'."));
	}
	
	
	public static BaseComponent[] formatMessage(String text) {
		return TextComponent.fromLegacyText(net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', text));
	}
}
